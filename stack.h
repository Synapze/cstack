#ifndef __STACK_H__
#define __STACK_H__

#include <stdlib.h>
#include <err.h>

typedef struct _Stack
{
    
    void* head;

} Stack;

Stack* new_stack();

void  push_stack(Stack *stack, void* data);
void* pop_stack(Stack *stack);
void* peek_stack(Stack *stack);

int is_stack_empty(Stack *stack);

void free_stack(Stack* stack);

#endif
