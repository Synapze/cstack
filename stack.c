#include "stack.h"

// Define intern structure
typedef struct _StackNode
{
        struct _StackNode* next;
            void* data;

} StackNode;

/*
 * Return a new fresh and clean stack 100% Vegan, Bio, Organic and water free
 */
Stack* new_stack() {

    Stack* stack = (Stack *) malloc(sizeof(Stack));
    stack->head = NULL;

    return stack;
}

/*
 * Push a new element
 */
void push_stack(Stack *stack, void* data)
{

    StackNode* node = (StackNode *) malloc(sizeof(StackNode));
    node->data = data;
    node->next = stack->head;
    
    // Transplant the new head
    stack->head = node;
}

/*
 * Pop the head and remove it i.e cut the head (18+) and send back the brain tag
 */
void* pop_stack(Stack *stack)
{
    
    if (is_stack_empty(stack))
        errx(1, "Stack is empty, please stop digging it hurts (pop)");

    StackNode* head = stack->head;

    // Regrow a new head (Hydra powa)
    stack->head = head->next;

    // Remove the brain tag, blood expected
    void* data = head->data;

    // Throw away the old head, avoid common trash use acid
    free(head);

    // Send the brain tag
    return data;
}

/*
 * Peek the head i.e a small incision to watch what the brain look like
 */
void* peek_stack(Stack *stack)
{
    if (is_stack_empty(stack))
        errx(1, "Please stop peek in dead body, u weirdo ! (peek)");

    // Surgical procedure
    return ((StackNode *) (stack->head))->data;
}

/*
 * Check pulse
 */
int is_stack_empty(Stack *stack)
{
    // Is it already dead ?
    return stack->head == NULL;
}

/*
 * Acid bath no proof no jail
 */
void free_stack(Stack* stack)
{

    StackNode* node = stack->head;
    StackNode* tmp;

    // Destroy all the head
    while (node != NULL) {
        tmp = node->next;
        free(node);
        node = tmp;
    }

    // Free empty body;
    free(node);

    // Throw away the body
    free(stack);
}
