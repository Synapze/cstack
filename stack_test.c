#include <stdio.h>
#include <stdlib.h>

#include "stack.h"

int main()
{
    // Create a new stack
    Stack* stack = new_stack();
    
    // Print stack addr
    printf("Stack addr : %p\n", stack);

    // Seeding
    srand(0);

    printf("\n");

    // Pushing few random element
    size_t i;
    double* element;
    for (i = 0; i < 10; ++i)
    {
        element = malloc(sizeof(double));
        *element = rand();

        // Pushing element
        push_stack(stack, element);
    
        printf("Push %f (%p) in stack %p\n", *element, element, stack);
    }

    printf("\n");

    // Peek
    element = (double *) peek_stack(stack);
    
    printf("Peek %f (%p) in stack %p\n", *element, element, stack);

    printf("\n");

    // Poping all element
    while (!is_stack_empty(stack))
    {
        element = (double *) pop_stack(stack);
        
        printf("Pop %f (%p) from stack %p\n", *element, element, stack);
    
        free(element);
    }

    // Free the stack
    free_stack(stack);
}
